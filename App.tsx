/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  Button
} from 'react-native';

import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './app/modules/login/login';
import MainScreen from './app/main';
import { navigationRef } from './app/config/RootNavigation';
import { getSession } from './app/shared/reducers/authentication';
import {navigate,openMenu} from './app/config/RootNavigation';
import { DrawerActions } from '@react-navigation/native';
import deviceStorage from './app/config/deviceStorage';
import { setI18nConfig } from './app/language/translate-helper';
import { ChoiceLanguageModal } from './app/modules/home/modal-choice-lang';
import { setLocale } from './app/shared/reducers/locale';

const Stack = createStackNavigator();

const App = (props:any) => {

  const [showLang, setShowLang] = useState(false);
  const [langSelected, setLangSeleted] = useState(null);

  useEffect(() => {
    if (JSON.stringify(props.account) !== JSON.stringify({})) {
      const fetchData = async () => {
        const result = await deviceStorage.getItem('locale');
        setI18nConfig(result);
      }
      fetchData();
    }
  }, []);

  useEffect(() => {
    setI18nConfig(props.currentLocale);
  }, [props.currentLocale]);

  const showChoiceLang = () => {
    setShowLang(true);
  }

  const handlerCloseChoiceLang = () => {
    setShowLang(false);
  }

  const handleLocalizationChange = (codeLang) => {
    const fetchData = async () => {
      await deviceStorage.saveItem('locale',codeLang);
      setLangSeleted(codeLang);
      props.setLocale(codeLang);
    }
    fetchData();
    handlerCloseChoiceLang();
  };

  return (
    <>
    <ChoiceLanguageModal isShow={showLang} closeModal={handlerCloseChoiceLang} setLang={handleLocalizationChange}/>
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }}/>
        <Stack.Screen name="Main" component={MainScreen} options={{ 
          title: 'OneLife',
          headerLeft: (props) => (
            <Button
              onPress={()=>{openMenu()}}
              title="MENU"
              color="#841584"
            />
          ),
          headerRight: (props) => (
            <Button
              onPress={()=>{showChoiceLang()}}
              title="Language"
              color="#841584"
            />
          ),
        }}/>
      </Stack.Navigator>
    </NavigationContainer>
    </>
  );
};

const styles = StyleSheet.create({
  
});

const mapDispatchToProps = { getSession, setLocale };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

const mapStateToProps = ({  authentication, locale }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  currentLocale: locale.currentLocale
});

export default connect(
  mapStateToProps,
  mapDispatchToProps)(App);
