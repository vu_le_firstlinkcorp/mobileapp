/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import initStore from './app/config/store';
import { Provider } from 'react-redux';
import React from 'react';
import { bindActionCreators } from 'redux';
import setupAxiosInterceptors from './app/config/axios-interceptor';
import { clearAuthentication } from './app/shared/reducers/authentication';
const store = initStore();
const actions = bindActionCreators({ clearAuthentication }, store.dispatch);
setupAxiosInterceptors(() => actions.clearAuthentication('login.error.unauthorized'));

const Root = () => (
    <Provider store={store}>
      <App />
    </Provider>
  )

AppRegistry.registerComponent("mobileApp", () => Root);
