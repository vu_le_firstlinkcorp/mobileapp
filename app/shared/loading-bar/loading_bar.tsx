import React, { Component } from 'react'
import { connect } from 'react-redux'

import { DEFAULT_SCOPE } from './loading_bar_ducks'
import { Image, View, Text, StyleSheet } from 'react-native';

class LoadingBar extends Component<any, any> {

  render() {
    const { loading, className } = this.props
    const show = loading > 0;

    if (!show) {
      return (<View></View>);
    }

    return (
      <View>
          <Text style={styles.loadingString}>.................</Text>
          {/* <Image
          source={{
                uri: 'https://reactnative.dev/img/tiny_logo.png',
          }} /> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  loadingString: {
    fontSize: 100,
    fontWeight: "900"
  }
})

const mapStateToProps = (state, ownProps) => ({
  loading: state.loadingBar[ownProps.scope || DEFAULT_SCOPE],
})

const ConnectedLoadingBar = connect(mapStateToProps)(LoadingBar)

export {
  LoadingBar,
  ConnectedLoadingBar as default,
}
