
export interface IAreas {
  id?: number;
  name?: string;
}

export const defaultValue: Readonly<IAreas> = {};
