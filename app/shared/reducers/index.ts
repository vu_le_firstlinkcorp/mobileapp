import { combineReducers } from 'redux';

// import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import { loadingBarReducer as loadingBar } from '../loading-bar/index';
import { popuptoastReducer as checkPopup } from '../modal-popup/index';
import areas, { AreasState } from '../../modules/areas/areas.reducer';
import locale, { LocaleState } from './locale';


export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly loadingBar: any;
  readonly checkPopup: any;
  readonly areas: AreasState;
  readonly locale: LocaleState;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  loadingBar,
  checkPopup,
  areas,
  locale
});

export default rootReducer;
