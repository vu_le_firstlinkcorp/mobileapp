
export const ACTION_TYPES = {
  SET_LOCALE: 'locale/SET_LOCALE',
};

const initialState = {
  currentLocale: '',
};

export type LocaleState = Readonly<typeof initialState>;

export default (state: LocaleState = initialState, action): LocaleState => {
  switch (action.type) {
    case ACTION_TYPES.SET_LOCALE: {
      const currentLocale = action.locale;
      return {
        currentLocale,
      };
    }
    default:
      return state;
  }
};

export const setLocale: (locale: string) => void = locale => async dispatch => {
  dispatch({
    type: ACTION_TYPES.SET_LOCALE,
    locale,
  });
};
