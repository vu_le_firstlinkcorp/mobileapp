import React, { useState } from 'react';
import { IRootState } from '../../shared/reducers';
import { connect } from 'react-redux';
import { showPopuptoast } from './popuptoast_ducks';
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from "react-native";

export interface IPopupProps extends StateProps, DispatchProps {}
const Popuptoast = (props: any) => {

  const { loading } = props;
  const show = loading && loading.return ? loading.return : false;

  const [modal, setModal] = useState(true);

  // const toggle = () => setModal(!modal);
  const toggle = () => {
    // setModal(false);
    props.showPopuptoast(null);
  }

  // const renderHTML = (rawHTML) => React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });

  if (show && show.scope && show.scope.length > 0) {
    return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={modal}
        >
          <View style={styles.centeredView}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text style={styles.modalText}>{show.scope}</Text>

                <TouchableHighlight
                  style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                  onPress={() => {
                    toggle();
                  }}
                >
                  <Text style={styles.textStyle}>Close</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>
    );
  } else {
    return (
      <View style={styles.hiddenPopup}></View>
    )
  }
}

const styles = StyleSheet.create({
  hiddenPopup: {
    display: "none"
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

const mapStateToProps = ({ checkPopup }: IRootState, ownProps) => ({
    loading: checkPopup,
    // loading: state.checkAdvertise[ownProps.scope || DEFAULT_SCOPE], // DIA HARD
  });
  
  type StateProps = ReturnType<typeof mapStateToProps>;

  const mapDispatchToProps = {
    showPopuptoast
  };

  type DispatchProps = typeof mapDispatchToProps;
  
  const ConnectedAdvertise = connect<StateProps>(
    mapStateToProps, mapDispatchToProps
    )(Popuptoast)
  
  export {
    Popuptoast as Popuptoast,
    ConnectedAdvertise as default,
  }
