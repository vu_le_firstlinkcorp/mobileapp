import PopuptoastContainer, { Popuptoast } from './popuptoast';

import popuptoastMiddleware from './popuptoast_middleware';
import { hidePopuptoast, popuptoastReducer, resetPopuptoast, showPopuptoast } from './popuptoast_ducks';
import ImmutablePopuptoast from './immutable';

export {
  hidePopuptoast,
  ImmutablePopuptoast,
  Popuptoast as popuptoast,
  popuptoastMiddleware,
  popuptoastReducer,
  resetPopuptoast,
  showPopuptoast
};
export default PopuptoastContainer;
