export const SHOW = 'popuptoast/SHOW';
export const HIDE = 'popuptoast/HIDE';
export const RESET = 'popuptoast/RESET';

export const DEFAULT_SCOPE = 'default';

export function showPopuptoast(scope = DEFAULT_SCOPE, status?) {
  return {
    type: SHOW,
    payload: {
      scope,status
    }
  };
}

export function hidePopuptoast(scope = DEFAULT_SCOPE) {
  return {
    type: HIDE,
    payload: {
      scope
    }
  };
}

export function resetPopuptoast(scope = DEFAULT_SCOPE) {
  return {
    type: RESET,
    payload: {
      scope
    }
  };
}

export function popuptoastReducer(state = {}, action: any = {}) {
  const { scope = DEFAULT_SCOPE } = action.payload || {};

  switch (action.type) {
    case SHOW:
      return {
        ...state,
        return: action.payload
      };
    case HIDE:
      return {
        ...state,
        [scope]: Math.max(0, (state[scope] || 1) - 1)
      };
    case RESET:
      return {
        ...state,
        [scope]: 0
      };
    default:
      return state;
  }
}
