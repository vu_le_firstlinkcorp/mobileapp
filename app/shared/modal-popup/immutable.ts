import { connect } from 'react-redux';
import popuptoast from './popuptoast';

import { DEFAULT_SCOPE } from './popuptoast_ducks';

const mapImmutableStateToProps = (state, ownProps) => ({
  loading: state.get('checkPopup')[ownProps.scope || DEFAULT_SCOPE] // DIE HARD
});

export default connect(mapImmutableStateToProps)(popuptoast);
