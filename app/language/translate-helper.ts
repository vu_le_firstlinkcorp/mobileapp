import { memoize } from "lodash";
import i18n from 'i18n-js';
import { I18nManager } from "react-native";

import * as RNLocalize from 'react-native-localize';

const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    vi: () => require('../i18n/vi.json'),
    en: () => require('../i18n/en.json'),
};

export const translate = memoize(
    (key, config?) => i18n.t(key, config),
    (key, config?) => (config ? key + JSON.stringify(config) : key)
);

export const setI18nConfig = (codeLang) => {
    // fallback if no available language fits
    if (codeLang === 'vi' || codeLang === 'en') {
        // const fallback = { languageTag: 'vi', isRTL: false };
  
        // const { languageTag, isRTL } = RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) || fallback;
    
        const languageTag = codeLang;
        const isRTL = false;
    
        // clear translation cache
        translate.cache.clear();
        // update layout direction
        I18nManager.forceRTL(isRTL);
        // set i18n-js config
        i18n.translations = { [languageTag]: translationGetters[languageTag]() };
        console.log('lang is: ', i18n.translations);
        i18n.locale = languageTag;
    } else {
        console.log('lang error: ' + codeLang);
    }
};