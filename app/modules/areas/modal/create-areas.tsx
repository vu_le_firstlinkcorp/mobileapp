import React, { Component, useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from "react-native";
import { createEntity, updateEntity } from '../areas.reducer';
import { TextInput } from "react-native-gesture-handler";
import { IRootState } from "app/shared/reducers";
import { connect } from "react-redux";
import { IAreas } from "app/shared/model/areas.model";

export const CreateAreaModal = (props: any) => {
    const [areasName, setAreasName] = useState('');

    const handleChangeAreaName = (name) => {
        setAreasName(name.nativeEvent.text);
    }

    const handleSaveArea = () => {
        const areaEntity: IAreas = {};
        areaEntity.name = areasName;
        if (props.area && props.area.id) {
            areaEntity.id = props.area.id;
            props.updateEntity(areaEntity);
        } else {
            props.createEntity(areaEntity);
        }
    }

  return (
    <Modal
        animationType="slide"
        transparent={true}
        visible={props.showModalCreate}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
    >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TextInput
                style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 15}}
                onChange={text => handleChangeAreaName(text)}
                placeholder={'input area name'}
                defaultValue={props.area && props.area.name ? props.area.name : ''}
            />

            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "white" }}
              onPress={() => {
                props.handleCloseModal();
              }}
            >
              <Text style={{color: "#2196F3"}}>Cancel</Text>
            </TouchableHighlight>

            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={() => {
                handleSaveArea();
              }}
            >
              <Text style={styles.textStyle}>Save</Text>
            </TouchableHighlight>
          </View>
        </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

const mapDispatchToProps = {
    createEntity,
    updateEntity
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

const mapStateToProps = ({ areas }: IRootState) => ({
    areasList: areas.entities
});

export default connect(
  mapStateToProps,
  mapDispatchToProps)(CreateAreaModal);
