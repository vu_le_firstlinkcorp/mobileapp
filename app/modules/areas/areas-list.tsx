/**
 * Main screen after login action
 *
 * @format
 */

import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Image, Button, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { IRootState } from '../../shared/reducers';
import { getSearchEntities, getEntities, deleteEntity } from './areas.reducer';
import CreateAreaModal from './modal/create-areas';
import { translate } from '../../language/translate-helper';

export const AreaList = (props:any) => {

    const [showModalCreate, setShowModalCreate] = useState(false);
    const [idSelected, setIdSeleted] = useState(null);

    useEffect(() => {
        props.getEntities();
    }, []);

    useEffect(() => {
        if (props.updateSuccess) {
            props.getEntities();
            setShowModalCreate(false);
        }
    }, [props.updateSuccess]);

    const handleShowModal = (idSelect) => {
        setShowModalCreate(true);
        setIdSeleted(idSelect);
    }

    const handleCloseModal = () => {
        setShowModalCreate(false);
    }

    return (
        <>
            <View style={{flex: 1, flexDirection: 'row', alignContent: 'flex-start', flexWrap: 'wrap'}}>
                <View style={{width: '100%', height: 50}}>
                    <Button
                        onPress={()=>{handleShowModal(null)}}
                        title={translate('areas-screen.create-areas')}
                        color="#841584"
                    />
                </View>
                <View style={{width: '20%', height: 50}}>
                    <Text>Action</Text>
                </View>
                <View style={{width: '20%', height: 50}}>
                    <Text>ID</Text>
                </View>
                <View style={{width: '60%', height: 50}}>
                    <Text>NAME</Text>
                </View>
                {props.areasList && props.areasList.length > 0 ? 
                    props.areasList.map((area, i) => (
                        <View key={`entity-${i}`} style={{width: '100%', height: 60, flexDirection: 'row'}}>
                            <View style={{width: '10%', height: 50}}>
                                <TouchableHighlight
                                    activeOpacity={0.6}
                                    underlayColor="#DDDDDD"
                                    onPress={() => handleShowModal(area)}>
                                    <Image 
                                        source={require('../../content/image/iconEdit.png')}
                                        style={{width:20, height:20}}
                                    />
                                </TouchableHighlight>
                            </View>
                            <View style={{width: '10%', height: 50}}>
                                <TouchableHighlight
                                    activeOpacity={0.6}
                                    underlayColor="#DDDDDD"
                                    onPress={() => props.deleteEntity(area.id)}>
                                    <Image 
                                        source={require('../../content/image/iconDelete.png')}
                                        style={{width:20, height:20}}
                                    />
                                </TouchableHighlight>
                            </View>
                            <View style={{width: '20%', height: 50}}>
                                <Text>{area.id}</Text>
                            </View>
                            <View style={{width: '60%', height: 50}}>
                                <Text>{area.name}</Text>
                            </View>
                        </View>
                    ))
                    : (
                        <Text>No data</Text>
                )}
            </View>
            <CreateAreaModal area={idSelected} showModalCreate={showModalCreate} handleCloseModal={handleCloseModal}/>
        </>
    );
};

const styles = StyleSheet.create({
  
});

const mapDispatchToProps = {
    getSearchEntities,
    getEntities,
    deleteEntity
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

const mapStateToProps = ({ areas, locale }: IRootState) => ({
    areasList: areas.entities,
    updateSuccess: areas.updateSuccess,
    currentLocale: locale.currentLocale
});

export default connect(
  mapStateToProps,
  mapDispatchToProps)(AreaList);
