import axios from 'axios';

import { cleanEntity } from '../../shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from '../../shared/reducers/action-type.util';

import { IAreas, defaultValue } from '../../shared/model/areas.model';

import { ICrudSearchAction, ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from '../../shared/entity-redux-type';

export const ACTION_TYPES = {
  SEARCH_AREAS: 'areas/SEARCH_AREAS',
  FETCH_AREAS_LIST: 'areas/FETCH_AREAS_LIST',
  FETCH_AREAS: 'areas/FETCH_AREAS',
  CREATE_AREAS: 'areas/CREATE_AREAS',
  UPDATE_AREAS: 'areas/UPDATE_AREAS',
  DELETE_AREAS: 'areas/DELETE_AREAS',
  RESET: 'areas/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IAreas>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type AreasState = Readonly<typeof initialState>;

// Reducer

export default (state: AreasState = initialState, action): AreasState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.SEARCH_AREAS):
    case REQUEST(ACTION_TYPES.FETCH_AREAS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_AREAS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_AREAS):
    case REQUEST(ACTION_TYPES.UPDATE_AREAS):
    case REQUEST(ACTION_TYPES.DELETE_AREAS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.SEARCH_AREAS):
    case FAILURE(ACTION_TYPES.FETCH_AREAS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_AREAS):
    case FAILURE(ACTION_TYPES.CREATE_AREAS):
    case FAILURE(ACTION_TYPES.UPDATE_AREAS):
    case FAILURE(ACTION_TYPES.DELETE_AREAS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.SEARCH_AREAS):
    case SUCCESS(ACTION_TYPES.FETCH_AREAS_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_AREAS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_AREAS):
    case SUCCESS(ACTION_TYPES.UPDATE_AREAS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_AREAS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/areas';
const apiSearchUrl = 'api/_search/areas';

// Actions

export const getSearchEntities: ICrudSearchAction<IAreas> = (query, page, size, sort) => ({
  type: ACTION_TYPES.SEARCH_AREAS,
  payload: axios.get<IAreas>(`${apiSearchUrl}?query=${query}${sort ? `&page=${page}&size=${size}&sort=${sort}` : ''}`),
});

export const getEntities: ICrudGetAllAction<IAreas> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_AREAS_LIST,
    payload: axios.get<IAreas>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IAreas> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_AREAS,
    payload: axios.get<IAreas>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IAreas> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_AREAS,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IAreas> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_AREAS,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IAreas> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_AREAS,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});