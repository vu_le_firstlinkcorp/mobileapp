/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  TouchableHighlight,
  Platform,
  NativeModules,
} from 'react-native';

import { login } from '../../shared/reducers/authentication';

import { connect } from 'react-redux';
import { IRootState } from '../../shared/reducers';

import {navigate} from '../../config/RootNavigation';

import { getSession } from '../../shared/reducers/authentication';
import LoadingBar from '../../shared/loading-bar/index';
import Popuptoast from '../../shared/modal-popup/popuptoast';
import deviceStorage from '../../config/deviceStorage';
import { setI18nConfig } from '../../language/translate-helper';
import * as RNLocalize from 'react-native-localize';

export interface IUserSettingsProps extends StateProps, DispatchProps {}

export const Login = (props:any,{navigation}:any) => {

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loginMessage, setLoginMessage] = useState('');

  const handleLogin = () => {
    props.login(username, password, false, 'en');
  };

  useEffect(() => {
    props.getSession();
  }, []);

  useEffect(() => {
    if (JSON.stringify(props.account) !== JSON.stringify({})) {
      navigate('Main', {
        ...props
      });
      // const result = props.account.langKey;
      const result = 
      Platform.OS === 'ios'
        ? NativeModules.SettingsManager.settings.AppleLocale
        : NativeModules.I18nManager.localeIdentifier;
      setI18nConfig(result.substring(0, 2));
      navigate('Main');
    } else {
      setLoginMessage('ahuhu');
    }
  }, [props.account]);

  const handleChangeUsername = (username: any) => {
    setUsername(username.nativeEvent.text);
  }

  const handleChangePassword = (password: any) => {
    setPassword(password.nativeEvent.text);
  }

  return (
    <>
      <View style={styles.mainBlock}>
        <LoadingBar/>
        <Popuptoast />
        <View style={styles.loginBlock}>
          <View style={[styles.logoBlock, styles.centerElement]}>
            <Image 
                source={require('../../content/image/logoApp.png')}
                style={{height: '100%', width: '45%'}}
            />
          </View>
          <View style={[styles.wellcomeBlock, styles.centerElement]}>
            <Text style={[styles.wellcomeTitle]}>Welcome to OneLife</Text>
            <Text style={styles.wellcomeContent}>Build for system for your life</Text>
          </View>
          <View style={[styles.inputBlock, styles.centerElement]}>
            <View style={[styles.usernameInput]}>
              <Image 
                  source={require('../../content/image/iconUser.png')}
                  style={{width:'10%', height:'80%'}}
              />
              <TextInput
                style={{height:40,width:'80%', marginLeft: 10}}
                placeholder={'input username'}
                onChange={text => handleChangeUsername(text)}
            />
            </View>

            <View style={[styles.usernameInput]}>
              <Image 
                  source={require('../../content/image/iconPass.png')}
                  style={{width:'10%', height:'80%'}}
              />
              <TextInput
                style={{height:40,width:'80%', marginLeft: 10}}
                placeholder={'input password'}
                secureTextEntry={true}
                onChange={text => handleChangePassword(text)}
            />
            </View>
          </View>
          <View style={[styles.buttonBlock, styles.centerElement]}>
            <TouchableHighlight
              style={styles.buttonLogin}
              onPress={() => {}}>
              <Button
                onPress={()=>{handleLogin()}}
                title="SIGN IN"
                color="white"
              />
            </TouchableHighlight>
            <Text>Forgot password?</Text>
          </View>
        </View>
      </View>
      {/* <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <LoadingBar/>
          <Popuptoast />
          <View>
            <View>
              <Text>Username: </Text>
              <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChange={text => handleChangeUsername(text)}
              />
            </View>
            <View>
              <Text>Password: </Text>
              <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                secureTextEntry={true}
                onChange={text => handleChangePassword(text)}
              />
            </View>
            <View>
            <Button
              onPress={()=>{handleLogin()}}
              title="Login"
              color="#841584"
            />
            </View>
            <View>
              <Text>Status: {loginMessage}</Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView> */}
    </>
  );
};

const styles = StyleSheet.create({
  mainBlock: {
    flex: 1, 
    flexDirection: 'column', 
    justifyContent: 'center', 
    flexWrap: 'wrap',
    backgroundColor: 'white',
  },
  loginBlock: {
    width: '100%', 
    height: '60%',
  },
  logoBlock: {
    height: '25%', 
  },
  wellcomeBlock: {
    height: '25%', 
  },
  usernameInput: {
    width: '80%', 
    flexDirection: 'row',
    borderColor: 'gray', 
    borderWidth: 1,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  inputBlock: {
    height: '25%', 
  },
  buttonBlock: {
    height: '25%'
  },
  centerElement: {
    width: '100%',
    justifyContent: 'center', 
    alignItems: 'center'
  },
  wellcomeTitle: {
    fontSize: 30,
    fontWeight: '900'
  },
  wellcomeContent: {
    fontSize: 20,
  },
  buttonLogin: {
    width:'60%',
    borderColor: '#2E7FF7',
    borderWidth: 1,
    backgroundColor: '#2E7FF7',
    borderRadius: 25
  }
});

const mapDispatchToProps = { login, getSession };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

const mapStateToProps = ({  authentication }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  loginError: authentication.loginError,
  loginSuccess: authentication.loginSuccess,
  account: authentication.account
});

export default connect(
  mapStateToProps,
  mapDispatchToProps)(Login);
