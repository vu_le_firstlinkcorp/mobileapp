import React, { useLayoutEffect, useEffect } from 'react';
import { connect } from 'react-redux';

import { View, Text } from 'react-native';
import { IRootState } from '../../shared/reducers';
import { logout } from '../../shared/reducers/authentication';
import { navigate } from '../../config/RootNavigation';

export interface ILogoutProps extends StateProps, DispatchProps {
  idToken: string;
  logoutUrl: string;
}

export const Logout = (props: ILogoutProps) => {

  useEffect(() => {
    if (props.account) {
      if (JSON.stringify(props.account) === JSON.stringify({})) {
        navigate('Login');
      }
    }
  }, [props.account]);

  useLayoutEffect(() => {
    props.logout();
    const logoutUrl = props.logoutUrl;
    if (logoutUrl) {
      // if Keycloak, logoutUrl has protocol/openid-connect in it
      // window.location.href = logoutUrl.includes('/protocol')
      //   ? logoutUrl + '?redirect_uri=' + window.location.origin
      //   : logoutUrl + '?id_token_hint=' + props.idToken + '&post_logout_redirect_uri=' + window.location.origin;
    }
  });

  return (
    <View>
      <Text>Logged out successfully!</Text>
    </View>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  logoutUrl: storeState.authentication.logoutUrl,
  idToken: storeState.authentication.idToken,
  account: storeState.authentication.account
});

const mapDispatchToProps = { logout };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
