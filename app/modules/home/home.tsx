/**
 * Main screen after login action
 *
 * @format
 */

import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { connect } from 'react-redux';
import { IRootState } from '../../shared/reducers';
import { translate, setI18nConfig } from '../../language/translate-helper';

export const Home = (props:any) => {

  // useEffect(() => {
  //   // setI18nConfig(props.currentLocale);
  //   console.log("lang refresh: " + props.currentLocale);
  // }, [props.currentLocale]);

  return (
    <>
      <View>
        <Text>{translate('home-screen.hello-here')} {props.account.firstName} {props.account.lastName}</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  
});

const mapDispatchToProps = {  };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

const mapStateToProps = ({  authentication, locale }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  loginError: authentication.loginError,
  loginSuccess: authentication.loginSuccess,
  account: authentication.account,
  currentLocale: locale.currentLocale
});

export default connect(
  mapStateToProps,
  mapDispatchToProps)(Home);
