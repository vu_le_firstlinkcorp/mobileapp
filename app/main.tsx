/**
 * Main screen after login action
 *
 * @format
 */

import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { connect } from 'react-redux';
import { IRootState } from './shared/reducers';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './config/RootNavigation';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './modules/home/home';
import { createDrawerNavigator } from '@react-navigation/drawer';
import AreaList from './modules/areas/areas-list';
import Logout from './modules/login/logout';
import { LoadingBar } from './shared/loading-bar/loading_bar';
import { Popuptoast } from './shared/modal-popup/popuptoast';

const Drawer = createDrawerNavigator();

export const MainScreen = (props:any) => {

  return (
    <>
    <LoadingBar/>
    <Popuptoast/>
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={Home}/>
      <Drawer.Screen name="AreaList" component={AreaList}/>
      <Drawer.Screen name="Logout" component={Logout} />
      <Drawer.Screen name="English" component={Home}
        listeners={{
          focus: e => {
            // Prevent default action
            console.log('xxxxx');
          },
        }}
      />
      <Drawer.Screen name="Vietnamese" component={Logout} />
    </Drawer.Navigator>
    </>
  );
};

const styles = StyleSheet.create({
  
});

const mapDispatchToProps = {  };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

const mapStateToProps = ({  authentication }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  loginError: authentication.loginError,
  loginSuccess: authentication.loginSuccess,
  account: authentication.account,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps)(MainScreen);
