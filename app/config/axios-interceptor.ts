import axios from 'axios';

import { SERVER_API_URL } from './constants';
// import { Storage } from './storage/storage-util';

import devieStorage from '../config/deviceStorage';
import AsyncStorage from '@react-native-community/async-storage';

const TIMEOUT = 1 * 60 * 1000;
axios.defaults.timeout = TIMEOUT;
axios.defaults.baseURL = SERVER_API_URL;

const setupAxiosInterceptors = (onUnauthenticated: any) => {
  const onRequestSuccess = async (config: any) => {
    const token = await devieStorage.getItem('jhi-authenticationToken');
    console.log(token);
    const lang = devieStorage.getItem('locale');
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    config.headers.locale = '10';
    if (lang) {
      config.headers.locale = '10';
    }
    return config;
  };
  const onResponseSuccess = (response: any) => response;
  const onResponseError = (err: any) => {
    const status = err.status || (err.response ? err.response.status : 0);
    if (status === 403 || status === 401) {
      onUnauthenticated();
    }
    return Promise.reject(err);
  };
  axios.interceptors.request.use(onRequestSuccess);
  axios.interceptors.response.use(onResponseSuccess, onResponseError);

  const getData = async (key) => {
    try {
      const value = await devieStorage.getItem(key);
  
      return value;
    } catch (error) {
      console.error(error)
    }
  }
};

export default setupAxiosInterceptors;
