import { isPromise } from './constants';
import { showPopuptoast } from '../shared/modal-popup/popuptoast_ducks';

const addErrorAlert = (message, key?, data?) => {
  key = key ? key : message;
  console.log("error: " + key);
};
export default ({dispatch}) => next => action => {
  // If not a promise, continue on
  if (!isPromise(action.payload)) {
    return next(action);
  }

  /**
   *
   * The notification middleware serves to dispatch the initial pending promise to
   * the promise middleware, but adds a `then` and `catch.
   */
  return next(action)
    .then(response => {
      if (action.meta && action.meta.successMessage) {
        if (action.meta.successMessage.props) {
          dispatch(showPopuptoast(action.meta.successMessage.props.dangerouslySetInnerHTML.__html, 'success'));
        } else {
          dispatch(showPopuptoast(action.meta.successMessage, 'success'));
        }
        // toast.success(action.meta.successMessage);
      } else if (response && response.action && response.action.payload && response.action.payload.headers) {
        const headers = response.action.payload.headers;
        let alert: string = null;
        let alertParams: string = null;
        Object.entries(headers).forEach(([k, v]: [string, string]) => {
          if (k.toLowerCase().endsWith('app-alert')) {
            alert = v;
          } else if (k.toLowerCase().endsWith('app-params')) {
            alertParams = v;
          }
        });
        if (alert) {
          const alertParam = alertParams;
          // toast.success(translate(alert, { param: alertParam }));
        }
      }
      return Promise.resolve(response);
    })
    .catch(error => {
      if (action.meta && action.meta.errorMessage) {
        if (action.meta.errorMessage.props) {
          dispatch(showPopuptoast(action.meta.errorMessage.props.dangerouslySetInnerHTML.__html));
        } else {
          dispatch(showPopuptoast(action.meta.errorMessage));
        }
      } else if (error && error.response) {
        const response = error.response;
        const data = response.data;
        if (!(response.status === 401 && (error.message === '' || (data && data.path && data.path.includes('/api/account'))))) {
          let i;
          switch (response.status) {
            // connection refused, server not reachable
            case 0:
              dispatch(showPopuptoast('error.servernotreachable'));
              break;

            case 400: {
              const headers = Object.entries(response.headers);
              let errorHeader = null;
              let entityKey = null;
              headers.forEach(([k, v]: [string, string]) => {
                if (k.toLowerCase().endsWith('app-error')) {
                  errorHeader = v;
                } else if (k.toLowerCase().endsWith('app-params')) {
                  entityKey = v;
                }
              });
              if (errorHeader) {
                const entityName = 'global.menu.entities.' + entityKey;
                addErrorAlert(errorHeader, errorHeader, { entityName });
              } else if (data !== '' && data.fieldErrors) {
                const fieldErrors = data.fieldErrors;
                for (i = 0; i < fieldErrors.length; i++) {
                  const fieldError = fieldErrors[i];
                  if (['Min', 'Max', 'DecimalMin', 'DecimalMax'].includes(fieldError.message)) {
                    fieldError.message = 'Size';
                  }
                  // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                  const convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                  const fieldName = `oneLifeApp.${fieldError.objectName}.${convertedField}`;
                  addErrorAlert(`Error on field "${fieldName}"`, `error.${fieldError.message}`, { fieldName });
                }
              } else if (data !== '' && data.message) {
                addErrorAlert(data.message, data.message, data.params);
              } else {
                dispatch(showPopuptoast(data));
              }
              break;
            }
            case 404:
              dispatch(showPopuptoast('error.urlnotfound'));
              break;
            case 401:
              dispatch(showPopuptoast('Login fail'));
              break;
            case 200:
              dispatch(showPopuptoast('Success'));
              break;
            default:
              // if (data !== '' && data.message) {
              //   addErrorAlert(data.message);
              // } else {
              //   addErrorAlert(data);
              // }
              dispatch(showPopuptoast('error.commonerror'));
          }
        }
      } else if (error && error.config && error.config.url === 'api/account' && error.config.method === 'get') {
        /* eslint-disable no-console */
        if (error && error.message && error && error.message === "Network Error") {
          dispatch(showPopuptoast(error.message));
          return;
        } else {
          dispatch(showPopuptoast('Authentication Error: Trying to access url api/account with GET.'));
        }
      } else if (error && error.message) {
        dispatch(showPopuptoast(error.message));
      } else {
        dispatch(showPopuptoast('Unknown error!'));
      }
      return Promise.reject(error);
    });
};
