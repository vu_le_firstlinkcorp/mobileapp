import AsyncStorage from '@react-native-community/async-storage';
import { isPromise } from './constants';

const deviceStorage = {
  
    async saveItem(key: string, value: string) {
      try {
        if (!isPromise(value)) {
          const jsonValue = JSON.stringify(value);
          await AsyncStorage.setItem(key, jsonValue);
        }
      } catch (error) {
        console.log('AsyncStorage Error: ' + error.message);
      }
    },

    async getItem(key: string) {
      try {
        const jsonValue = await AsyncStorage.getItem(key);
        // console.log("value of " + "key: " + jsonValue);
        return jsonValue != null ? JSON.parse(jsonValue) : null;
      } catch (error) {
        console.log('AsyncStorage Error: ' + error.message);
      }
    },

    async removeItem(key: string) {
      try {
        await AsyncStorage.removeItem(key);
      } catch (error) {
        console.log('AsyncStorage Error: ' + error.message);
      }
    },
    
};

export default deviceStorage;