/**
 * Check if the passed object is a promise
 * @param value the object to check
 * Promise
 * Bearer
 */
export const isPromise = (value: any) => {
    const isPromise = value instanceof Promise;
    if (isPromise) {
        return true;
    } else {
        return false;
    }
};
export const SERVER_API_URL = "http://localhost:8080/";